$(document).ready(function(){

  $('#menu-slider').slick({
    slidesToShow: 3,
    adaptiveHeight: true,
    prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button" style=""><span>Текущая неделя</span><svg width="70px" height="20px" viewBox="0 0 70 20" xml:space="preserve"><g id="arrow-shape" fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline id="line" points=" 5,10 65,10" preserveAspectRatio="none"/><polyline id="arrow" points=" 14,3 5,10 " /></g></svg></button>',
    nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style=""><span>Следующая неделя</span><svg width="70px" height="20px" viewBox="0 0 70 20" xml:space="preserve"><g id="arrow-shape" fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline id="line" points=" 5,10 65,10" preserveAspectRatio="none"/><polyline id="arrow" points=" 54,3 65,10 " /></g></svg></button>',
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });
});